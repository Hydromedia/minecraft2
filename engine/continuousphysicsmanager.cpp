#include "continuousphysicsmanager.h"

ContinuousPhysicsManager::ContinuousPhysicsManager(Graphics *g, World *w) : Manager(g, w)
{

}

ContinuousPhysicsManager::~ContinuousPhysicsManager()
{

}

void ContinuousPhysicsManager::onTick(float nanos){
    foreach(Entity *e, *m_entities)
    {
        e->onTick(nanos);
    }
    collide();
}

void ContinuousPhysicsManager::collide() {
    for (int i = 0; i < m_entities->count(); i++){
        Entity *e = m_entities->at(i);
        for (int j = i+1; j < m_entities->count(); j++){
            Entity *s = m_entities->at(j);
            Vector3 MTV = s->getShape()->collides(e->getShape());
            //Vector3 MTV2 = e->getShape()->collides(s->getShape());
            if (MTV != Vector3(0,0,0)) {
                std::cout << "Colliding" << MTV << "\n";
                s->setPosition(s->getPosition() + ((MTV)/2.0f));
                e->setPosition(e->getPosition() + ((Vector3(0,0,0) - MTV)/2.0f));
                s->onCollide(e);
                e->onCollide(s);
            }
        }
    }
}
