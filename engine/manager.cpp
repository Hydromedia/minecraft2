#include "manager.h"

Manager::Manager(Graphics *g, World *w)
{
    m_world = w;
    m_graphics = g;
}

Manager::~Manager()
{

}

void Manager::onTick(float nanos)
{
    foreach(Entity *e, *m_world->m_entities)
    {
        e->onTick(nanos);
    }
}
