#include "world.h"

World::World(Screen *s, Camera *camera, Graphics *g)
{
    m_graphics = g;
    m_screen = s;
    m_entities = new QList<Entity*>();
    m_managers = new QList<Manager*>();
    m_camera = camera;
}

World::~World()
{
    delete m_managers;
    delete m_entities;
}

void World::onTick(float nanos){
    foreach(Entity *e, *m_entities)
    {
        e->onTick(nanos);
    }
//    foreach (Manager *m, *m_managers) {
//        m->onTick(nanos);
//    }
    //collide();
}

//void World::collide() {
//    for (int i = 0; i < m_entities->count(); i++){
//        Entity *e = m_entities->at(i);
//        for (int j = i+1; j < m_entities->count(); j++){
//            Entity *s = m_entities->at(j);
//            Vector3 MTV = s->getShape()->collides(e->getShape());
//            //Vector3 MTV2 = e->getShape()->collides(s->getShape());
//            if (MTV != Vector3(0,0,0)) {
//                std::cout << "Colliding" << MTV << "\n";
//                s->setPosition(s->getPosition() + ((MTV)/2.0f));
//                e->setPosition(e->getPosition() + ((Vector3(0,0,0) - MTV)/2.0f));
//                s->onCollide(e);
//                e->onCollide(s);
//            }
//        }
//    }
//}

void World::onDraw(Graphics *g){
    foreach(Entity *e, *m_entities)
    {
        e->onDraw(g);
    }
}

void World::onKeyPressed(QKeyEvent *event){

}

void World::onMouseDragged(QKeyEvent *event){

}

void World::resize(int w, int h){

}

void World::mousePressEvent(QMouseEvent *event){

}

void World::mouseMoveEvent(QMouseEvent *event){

}

void World::mouseReleaseEvent(QMouseEvent *event){

}

void World::wheelEvent(QWheelEvent *event){

}

void World::keyPressEvent(QKeyEvent *event){
    if (event->key() == Qt::Key_F) {
        m_camera->switchMode();
    }
}

void World::keyReleaseEvent(QKeyEvent *event){

}

void World::addEntity(Entity *e) {
    m_entities->append(e);
}

void World::removeEntity(Entity *e){
    m_entities->removeAll(e);
}
