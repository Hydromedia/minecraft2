#ifndef CAMERA_H
#define CAMERA_H
#include "vector.h"
#include <GL/glu.h>
#include "graphics.h"

class Graphics;
class Camera
{
public:
    Camera(Vector2 size, Graphics *gr);
    ~Camera();
    void switchMode();
    void transform(); // the camera does its transformations
    void translate(Vector3 vec); // change the position of the camera by vec
    void rotate(float yaw, float pitch); // change the direction the camera is looking
    float getYaw(); // just returns m yaw
    void resize(int y, int x);
    bool getMode();
    void setPosition(Vector3 vector);
    //void updateViewPlanes();
    void debugArray(float array[], int size);
    Vector3 m_look;
    Vector3 m_eye;

private:
    Vector3 m_realPosition;
    float m_yaw;        // rotation around the y axis (the vertical axis) – the horizontal direction
                        //you are looking in
    float m_pitch;      // angle with the horizontal plane – how much up or down you are looking
    float m_fov;        // field of view – how wide is the camera’s lens
    Vector2 m_ratio;    // size of the screen – the aspect ratio of the screen (look out for
                        //integer division)
    bool m_isThirdPerson = false;
    Graphics *g;

};


#endif // CAMERA_H
