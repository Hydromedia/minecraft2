#include "perlinnoisegenerator3d.h"
#include <math.h>       /* pow */
#include <stdlib.h>
#include <qpair.h>
#include <qhash.h>
#include <iostream>

PerlinNoiseGenerator3D::PerlinNoiseGenerator3D(int s)
{
    seed = s;
    srand(seed);
}

PerlinNoiseGenerator3D::~PerlinNoiseGenerator3D()
{

}

float PerlinNoiseGenerator3D::noise(float x, float z){
    float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    float ret = r * sin(x + z);
    ret = ret * sin(((float)seed)*(x+z));
    return ret;
    //int n = x + z * 57 + seed;
    //n = pow((n<<13), n);
    //return ( 1.f - ( (n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.f);
}

float PerlinNoiseGenerator3D::cosineInterpolate(float a, float b, float x){
    float ft = x * 3.1415927;
    float f = (1 - cos(ft)) * .5;
    //return  a*(1-x) + b*x;
    return  a*(1-f) + b*f;
}

float PerlinNoiseGenerator3D::smoothNoise(float x, float y){
    float corners = ( noise(x-1, y-1)+ noise(x+1, y-1)+ noise(x-1, y+1) + noise(x+1, y+1) ) / 16;
    float sides   = ( noise(x-1, y)  + noise(x+1, y)  + noise(x, y-1)  + noise(x, y+1) ) /  8;
    float center  =  noise(x, y) / 4;
    return corners + sides + center;
}

float PerlinNoiseGenerator3D::interpolatedNoise(float x, float z){

     int integerX = (int) x;
     float fractionalX = x - integerX;


     int integerZ = (int) z;
     float fractionalZ = z - integerZ;

     float v1 = smoothNoise(integerX, integerZ);
     float v2 = smoothNoise(integerX + 1, integerZ);
     float v3 = smoothNoise(integerX,     integerZ + 1);
     float v4 = smoothNoise(integerX + 1, integerZ + 1);

     float i1 = cosineInterpolate(v1 , v2 , fractionalX);
     float i2 = cosineInterpolate(v3 , v4 , fractionalX);

     float thing = cosineInterpolate(i1 , i2 , fractionalZ);
     return thing;

}


float PerlinNoiseGenerator3D::perlin3D(float x, float z, float wavelength){
    float persistence = .1;
    float Number_Of_Octaves = 1;
    float total = 0;
    float n = Number_Of_Octaves - 1;

    for (int i = 0; i <= n; i++){
        float frequency = .05;//((float) pow(2, i));//*4000;
        float amplitude = pow(persistence, i);
        total+=interpolatedNoise(x * frequency, z*frequency) * amplitude;
    }
    if (x == 1 && z == 1){
        x = 0;
    }
    return total; //interpolatedNoise(((float)x)/wavelength,((float)z)/wavelength);//total;
}
