#ifndef APPLICATION_H
#define APPLICATION_H
#include "graphics.h"
#include "screen.h"
#include "view.h"
#include <qgl.h>

class View;
class Camera;
class Screen;
class Application
{
public:
    Application(View *v, Vector2 size, Camera *c);
    ~Application();
    View * _view() { return view; }
    void onTick(float nanos);
    void onDraw(Graphics *g);
    void onKeyPressed(QKeyEvent *event);
    void onMouseDragged(QKeyEvent *event);
    virtual void initializeGL(Graphics *g) = 0;
    void resize(int w, int h);

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

    void wheelEvent(QWheelEvent *event);

    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void addScreen(Screen *screen);
    void removeTopScreen();
    void replaceScreen(Screen *newScreen);
protected:
    View *view;
    Camera *m_camera;
    Vector2 m_size;
    QList<Screen*> *m_screenList;

};

#endif // APPLICATION_H
