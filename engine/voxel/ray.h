#ifndef RAY_H
#define RAY_H
#include "src/vector.h"
#include "voxelmanager.h"
#include "voxelraycollision.h"
class Ray
{
public:
    Ray(Vector3 source, Vector3 direction);
    ~Ray();

    Vector3 m_source;
    Vector3 m_direction;

    //Vector3 voxelAbsolutePoint;
    //Vector3 voxelBlockPoint;
    Vector3 deltaT;
    Vector3 maxT;
    int stepX;
    int stepY;
    int stepZ;


    VoxelRayCollision collideChunk(VoxelManager *vox);
    float collideVoxelPlane(Vector3 normal, Vector3 point);
    void initializeChunkCollisions(int X, int Y, int Z);
};

#endif // RAY_H
