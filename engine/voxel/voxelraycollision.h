#ifndef VOXELRAYCOLLISION_H
#define VOXELRAYCOLLISION_H
#include "src/vector.h"


class VoxelRayCollision
{
public:
    VoxelRayCollision(Vector3 collisionPoint, Vector3 voxelCollisionCoordinate, Vector3 blockSide);
    ~VoxelRayCollision();

    Vector3 m_collisionPoint;
    Vector3 m_voxelCollisionCoordinate;
    Vector3 m_blockSide;

};

#endif // VOXELRAYCOLLISION_H
