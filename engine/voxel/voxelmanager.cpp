#include "voxelmanager.h"
#include "chunk.h";
#include <math.h>
#include <iterator>

VoxelManager::VoxelManager(Graphics *g, World *w) : Manager (g, w)
{

}

VoxelManager::~VoxelManager()
{

}


void VoxelManager::onTick(long seconds)
{
    if (!loadQueue.isEmpty()){
        Chunk *c = loadQueue.front();
        loadQueue.pop_front();
        float *startArray = new float[chunkSize*chunkSize];
        generateSingleChunkBlocks(c, c->m_position.x*chunkSize, c->m_position.y*chunkSize, c->m_position.z*chunkSize, startArray);
        initializeBlocks(c);
        c->resetVBO(m_world->m_graphics);
        chunks.insert(QPair<int, QPair<int, int> >(c->m_position.x, QPair<int, int>(c->m_position.y, c->m_position.z)), c);
    }
}

void VoxelManager::initializeWorld(int worldSX, int worldSY, int worldSZ, int chunkS){
    worldSizeX = worldSX;
    worldSizeY = worldSY;
    worldSizeZ = worldSZ;
    chunkSize = chunkS;
    for (int i = 0; i < worldSizeX; i++){
        for (int j = 0; j < worldSizeY; j++){
            for (int u = 0; u < worldSizeZ; u++){
                addChunk(new Chunk(chunkSize, Vector3(i, j, u)), i, j, u);
            }
        }
    }

    generateTerrain();
    initializeChunks();
}

Block VoxelManager::getBlock(int x, int y, int z)
{
    int a = floor(((float) x)/((float)chunkSize));
    int b = floor(((float) y)/((float)chunkSize));
    //the problem is that if z is negative, -1/32 rounds up to 0 instead of down
    int c = floor(((float) z)/((float)chunkSize));
    int aa = x-chunkSize*a;
    int bb = y-chunkSize*b;
    int cc = z-chunkSize*c;
    return getChunk(a, b, c)->
            blocks[(aa)+chunkSize*(bb+chunkSize*cc)];
    //blocks[i+ chunkSize * (j + chunkSize * u)]
}

void VoxelManager::setBlock(int x, int y, int z, Block b)
{
    int a = floor(((float) x)/((float)chunkSize));
    int b2 = floor(((float) y)/((float)chunkSize));
    int c = floor(((float) z)/((float)chunkSize));
    int aa = x-chunkSize*a;
    int bb = y-chunkSize*b2;
    int cc = z-chunkSize*c;
    getChunk(a, b2, c)->blocks[(aa)+chunkSize*(bb+chunkSize*cc)] = b;
}

void VoxelManager::loadAndUnloadChunks(int distance, Vector3 center)
{
    QHash< QPair<int, QPair<int, int> >, Chunk* > tempChunks;
    for (int x = center.x - distance; x <= center.x + distance; x++){
        for (int y = center.y - distance; y <= center.y + distance; y++){
            for (int z = center.z - distance; z <= center.z + distance; z++){
                if (hasChunk(x, y, z)) {
                    //add to temp because we already have it, and remove from current so we know whats left
                    tempChunks.insert(QPair<int, QPair<int, int> >(x, QPair<int, int>(y, z)), getChunk(x, y, z));
                    chunks.remove(QPair<int, QPair<int, int> >(x, QPair<int, int>(y, z)));
                } else {
                    //add it to the load queue
                    Chunk *c = new Chunk(chunkSize, Vector3(x, y, z));
                    loadQueue.push_front(c);
//                    float *startArray = new float[chunkSize*chunkSize];
//                    generateSingleChunkBlocks(c, c->m_position.x*chunkSize, c->m_position.y*chunkSize, c->m_position.z*chunkSize, startArray);
//                    initializeBlocks(c);
//                    c->resetVBO(m_world->m_graphics);
//                    tempChunks.insert(QPair<int, QPair<int, int> >(c->m_position.x, QPair<int, int>(c->m_position.y, c->m_position.z)), c);
                }
            }
        }
    }
    QHash< QPair<int, QPair<int, int> >, Chunk* >::iterator i;
    for (i = chunks.begin(); i != chunks.end(); ++i){
        delete *i;
    }
    chunks = tempChunks;
}

//void VoxelManager::generateTerrain(){

//}

bool VoxelManager::isInWorld(int x, int y, int z)
{
    if (hasChunk(floor(((float) x)/((float)chunkSize)),
                 floor(((float) y)/((float)chunkSize)),
                 floor(((float) z)/((float)chunkSize))))
    {
        if (y==128){
            return true;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

void VoxelManager::resetVBO(){

}

Chunk* VoxelManager::getChunk(int x, int y, int z)
{
    return chunks.value(QPair<int, QPair<int, int> >(x, QPair<int, int>(y, z)));
}

void VoxelManager::addChunk(Chunk *c, int x, int y, int z)
{
    chunks.insert(QPair<int, QPair<int, int> >(x, QPair<int, int>(y, z)), c);
}

bool VoxelManager::hasChunk(int x, int y, int z)
{
    return chunks.contains(QPair<int, QPair<int, int> >(x, QPair<int, int>(y, z)));
}

void VoxelManager::initializeChunks(){
    for (int i =0; i < worldSizeX; i++) {
        for (int j =0; j < worldSizeY; j++) {
            for (int u =0; u < worldSizeZ; u++) {
                initializeBlocks(getChunk(i, j, u));
                getChunk(i, j, u)->resetVBO(m_graphics);
            }
        }
    }
}

Vector3 VoxelManager::collide(Entity *e, Vector3 oldPosition, Vector3 potentialPosition)
{


    //Vector3(potentialPosition.x, chunkSize*worldSizeY - potentialPosition.y, potentialPosition.z);

    float width = e->boundingAABBSize.x;
    float height = e->boundingAABBSize.y;
    float depth = e->boundingAABBSize.z;

    ySweep(e, &oldPosition, &potentialPosition);
    xSweep(e, &oldPosition, &potentialPosition);
    zSweep(e, &oldPosition, &potentialPosition);
    return Vector3(oldPosition.x, oldPosition.y, oldPosition.z);
}

void VoxelManager::ySweep(Entity *e, Vector3 *oldPosition, Vector3 *potentialPosition)
{
    float height = e->boundingAABBSize.y;
    float width = e->boundingAABBSize.x;
    float depth = e->boundingAABBSize.z;

    //Y sweep
    if (oldPosition->y < potentialPosition->y){
        //going up
        int y = floor(oldPosition->y + height);
        while (y <= floor(potentialPosition->y + height)){
            for (int x = floor(oldPosition->x - width); x <= floor(oldPosition->x + width); x++) {
                for (int z = floor(oldPosition->z - depth); z <= floor(oldPosition->z + depth); z++) {
                    if (!isInWorld(x,y,z) || getBlock(x,y,z).type != 1) {
                        oldPosition->y = y-.001-height;//+height+.001;
                        e->m_acceleration.y = 0;
                        e->m_velocity.y = 0;
                        e->m_force.y = 0;
                        return;
                    }
                }
            }
            y++;
        }
        oldPosition->y = potentialPosition->y;
    } else if (oldPosition->y == potentialPosition->y){

    } else {
        //going down
        int y = floor(oldPosition->y - height) - 1;
        while (y >= floor(potentialPosition->y - height)){
            for (int x = floor(oldPosition->x - width); x <= floor(oldPosition->x + width); x++) {
                for (int z = floor(oldPosition->z - depth); z <= floor(oldPosition->z + depth); z++) {
                    if (!isInWorld(x,y,z) || getBlock(x,y,z).type != 1) {
                        oldPosition->y = y+.001+height + 1;//+height+.001;
                        e->m_acceleration.y = 0;
                        e->m_velocity.y = 0;
                        e->m_force.y = 0;
                        e->onStaticCollide(Vector3(0,1,0));
                        return;
                    }
                }
            }
            y--;
        }
        oldPosition->y = potentialPosition->y;
    }
}

void VoxelManager::zSweep(Entity *e, Vector3 *oldPosition, Vector3 *potentialPosition)
{
    float height = e->boundingAABBSize.y;
    float width = e->boundingAABBSize.x;
    float depth = e->boundingAABBSize.z;

    //Z sweep
    if (oldPosition->z < potentialPosition->z){
        //going up
        int z = floor(oldPosition->z + depth);
        while (z <= floor(potentialPosition->z + depth)){
            for (int y = floor(oldPosition->y - height); y <= floor(oldPosition->y + height); y++) {
                for (int x = floor(oldPosition->x - width); x <= floor(oldPosition->x + width); x++) {
                    if (!isInWorld(x,y,z) ||getBlock(x,y,z).type != 1) {
                        oldPosition->z = (z-.001-depth);//+height+.001;
                        e->m_acceleration.z = 0;
                        e->m_velocity.z = 0;
                        e->m_force.z = 0;
                        return;
                    }
                }
            }
            z++;
        }
        oldPosition->z = potentialPosition->z;
    } else if (oldPosition->z == potentialPosition->z){

    } else {
        //going down
        int z = floor(oldPosition->z - depth)-1;
        while (z >= floor(potentialPosition->z - depth)){
            for (int y = floor(oldPosition->y - height); y <= floor(oldPosition->y + height); y++) {
                for (int x = floor(oldPosition->x - width); x <= floor(oldPosition->x + width); x++) {
                    if (!isInWorld(x,y,z) ||getBlock(x,y,z).type != 1) {
                        oldPosition->z = z+.001+depth+1;//+height+.001;
                        e->m_acceleration.z = 0;
                        e->m_velocity.z = 0;
                        e->m_force.z = 0;
                        return;
                    }
                }
            }
            z--;
        }
        oldPosition->z = potentialPosition->z;
    }
}

void VoxelManager::xSweep(Entity *e, Vector3 *oldPosition, Vector3 *potentialPosition)
{
    float height = e->boundingAABBSize.y;
    float width = e->boundingAABBSize.x;
    float depth = e->boundingAABBSize.z;

    //X sweep
    if (oldPosition->x < potentialPosition->x){
        //going up
        int x = floor(oldPosition->x + width);
        while (x <= floor(potentialPosition->x + width)){
            for (int y = floor(oldPosition->y - height); y <= floor(oldPosition->y + height); y++) {
                for (int z = floor(oldPosition->z - depth); z <= floor(oldPosition->z + depth); z++) {
                    if (!isInWorld(x,y,z) ||getBlock(x,y,z).type != 1) {
                        oldPosition->x = x-.001-width;
                        e->m_acceleration.x = 0;
                        e->m_velocity.x = 0;
                        e->m_force.x = 0;
                        return;
                    }
                }
            }
            x++;
        }
        oldPosition->x = potentialPosition->x;
    } else if (oldPosition->x == potentialPosition->x){

    } else {
        //going down
        int x = floor(oldPosition->x - width)-1;
        while (x >= floor(potentialPosition->x - width)){
            for (int y = floor(oldPosition->y - height); y <= floor(oldPosition->y + height); y++) {
                for (int z = floor(oldPosition->z - depth); z <= floor(oldPosition->z + depth); z++) {
                    if (!isInWorld(x,y,z) || getBlock(x,y,z).type != 1) {
                        oldPosition->x = x+.001+width+1;//+height+.001;
                        e->m_acceleration.x = 0;
                        e->m_velocity.x = 0;
                        e->m_force.x = 0;
                        return;
                    }
                }
            }
            x--;
        }
        oldPosition->x = potentialPosition->x;
    }
}
