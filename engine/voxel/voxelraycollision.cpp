#include "voxelraycollision.h"

VoxelRayCollision::VoxelRayCollision(Vector3 collisionPoint, Vector3 voxelCollisionCoordinate, Vector3 blockSide)
{
    m_collisionPoint = collisionPoint;
    m_voxelCollisionCoordinate = voxelCollisionCoordinate;
    m_blockSide = blockSide;
}

VoxelRayCollision::~VoxelRayCollision()
{

}
