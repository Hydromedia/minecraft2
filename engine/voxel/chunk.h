#ifndef CHUNK_H
#define CHUNK_H

#include "engine/graphics.h";
#include "block.h";
#include <QGLFunctions>

class Chunk
{
public:
    Chunk(char size, Vector3 position);
    ~Chunk();
    Block *blocks;
    float *bufferData;
    void resetVBO(Graphics *g);
    void onDraw(Graphics *g);
    Vector3 m_position = Vector3(0,0,0);
    Block getBlock(int x, int y, int z);
protected:
    int m_numVerts = 0;
    char chunkSize;
    void addFace(Graphics *g, char texture, int numVerts, Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4);
    unsigned int bufferID = 0;

};

#endif // CHUNK_H
