#include "ray.h"
#include <math.h>

Ray::Ray(Vector3 source, Vector3 direction)
{
    m_source = source;
    direction.normalize();
    m_direction = direction;
}

Ray::~Ray()
{

}

void Ray::initializeChunkCollisions(int X, int Y, int Z)
{
    float tX;
    float tY;
    float tZ;

    if (stepX >= 0) {
      tX = (((float)(X+stepX)) - m_source.x)/(m_direction.x);
    } else {
      tX = (((float)(X)) - m_source.x)/(m_direction.x);
    }

    if (stepY >= 0) {
      tY = (((float)(Y+stepY)) - m_source.y)/(m_direction.y);
    } else {
      tY = (((float)(Y)) - m_source.y)/(m_direction.y);
    }

    if (stepZ >= 0) {
      tZ = (((float)(Z+stepZ)) - m_source.z)/(m_direction.z);
    } else {
      tZ = (((float)(Z)) - m_source.z)/(m_direction.z);
    }

    maxT = Vector3(tX, tY, tZ);

    float dX = fabs(1/m_direction.x);
    float dY = fabs(1/m_direction.y);
    float dZ = fabs(1/m_direction.z);
    deltaT = Vector3(dX, dY, dZ);
}

VoxelRayCollision Ray::collideChunk(VoxelManager *vox)
{
    Vector3 collisionPoint;
    Vector3 voxelCollisionCoordinate;
    Vector3 blockSide;
    int X;
    int Y;
    int Z;
    if (m_direction.x > 0) {
        X = floor(m_source.x);
        stepX = 1;
    } else if (m_direction.x < 0) {
        X = floor(m_source.x);
        stepX = -1;
    } else {
        X = floor(m_source.x);
        stepX = 0;
    }

    if (m_direction.y > 0) {
        Y = floor(m_source.y);
        stepY = 1;
    } else if (m_direction.y < 0) {
        Y = floor(m_source.y);
        stepY = -1;
    } else {
        Y = floor(m_source.y);
        stepY = 0;
    }

    if (m_direction.z > 0) {
        Z = floor(m_source.z);
        stepZ = 1;
    } else if (m_direction.z < 0) {
        Z = floor(m_source.z);
        stepZ = -1;
    } else {
        Z = floor(m_source.z);
        stepZ = 0;
    }

    blockSide = Vector3(0, 0, 0);
    voxelCollisionCoordinate = Vector3(X, Y, Z);
    collisionPoint = Vector3();


    while(vox->isInWorld(X, Y, Z) && vox->getBlock(X,Y,Z).type == 1){
        initializeChunkCollisions(X, Y, Z);
        if(maxT.x < maxT.y) {
            if(maxT.x < maxT.z && maxT.x == maxT.x) {
                X += stepX;
                maxT.x += deltaT.x;
                blockSide = Vector3(-stepX, 0, 0);
            } else if (maxT.z == maxT.z) {
                Z += stepZ;
                maxT.z += deltaT.z;
                blockSide = Vector3(0, 0, -stepZ);
            } else {
                break;
            }
        } else {
            if(maxT.y < maxT.z && maxT.y == maxT.y) {
                Y += stepY;
                maxT.y += deltaT.y;
                blockSide = Vector3(0, -stepY, 0);
            } else if (maxT.z == maxT.z) {
                Z += stepZ;
                maxT.z += deltaT.z;
                blockSide = Vector3(0, 0, -stepZ);
            } else {
                break;
            }
        }
        if(vox->isInWorld(X,Y,Z)){
            voxelCollisionCoordinate = Vector3(X, Y, Z);
        }
    }

    VoxelRayCollision col = VoxelRayCollision(collisionPoint, voxelCollisionCoordinate, blockSide);

    return col;
}
