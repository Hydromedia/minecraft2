#ifndef VOXELMANAGER_H
#define VOXELMANAGER_H
#include "engine/graphics.h"
#include "chunk.h"
#include <QHash>
#include <QPair>
#include <QList>
#include "engine/manager.h"
#include "engine/entity.h"
#include "block.h"

class VoxelManager : public Manager
{
public:
    VoxelManager(Graphics *g, World *w);
    ~VoxelManager();

    Vector3 collide(Entity *e, Vector3 oldPosition, Vector3 potentialPosition);
    virtual void onTick(long seconds);
    virtual void onDraw(Graphics *g) = 0;
    virtual void generateTerrain() = 0;
    virtual void generateBlocks(Chunk *c, int x, int y, int z, float *startArray) = 0;
    virtual void generateSingleChunkBlocks(Chunk *c, int x, int y, int z, float *startArray) = 0;
    virtual void initializeBlocks(Chunk*) = 0;
    bool isInWorld(int x, int y, int z);
    void resetVBO();
    void initializeWorld(int worldSX, int worldSY, int worldSZ, int chunkS);
    Chunk* getChunk(int x, int y, int z);
    Block getBlock(int x, int y, int z);
    void setBlock(int x, int y, int z, Block b);
    void loadAndUnloadChunks(int distance, Vector3 center);
    int chunkSize;
    int worldSizeX;
    int worldSizeY;
    int worldSizeZ;
    bool hasChunk(int x, int y, int z);
protected:
    void addChunk(Chunk* c, int x, int y, int z);
    void initializeChunks();
    QHash< QPair<int, QPair<int, int> >, Chunk* > chunks;
private:
    QList<Chunk*> loadQueue = QList<Chunk*>();
    void xSweep(Entity *e, Vector3 *oldPosition, Vector3 *potentialPosition);
    void ySweep(Entity *e, Vector3 *oldPosition, Vector3 *potentialPosition);
    void zSweep(Entity *e, Vector3 *oldPosition, Vector3 *potentialPosition);

};

#endif // VOXELMANAGER_H
