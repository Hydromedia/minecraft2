#include "camera.h"
#include <math.h>

Camera::Camera(Vector2 size, Graphics *gr)
{
    m_eye = Vector3(0,0,0); // Start the camera eye at the origin with a "player height" of 2 meters
    m_realPosition = Vector3(0,0,0);
    m_yaw = 0; // Look down the -Z axis
    m_pitch = 0; // Look horizontally
    m_fov = 60; // 60 degrees is a fairly standard field of view
    m_ratio = size; // Save the screen size
    g=gr;

}

Camera::~Camera()
{

}

void Camera::transform() // the camera does its transformations
{
    glMatrixMode(GL_PROJECTION); // use the projection matrix
    glLoadIdentity(); // reset the matrix, since gluPerspective multiplies it
    gluPerspective(m_fov, (float)m_ratio.x/(float)m_ratio.y, .1f, 900);
    glMatrixMode(GL_MODELVIEW); // use the modelview matrix
    glLoadIdentity(); // reset the matrix, since gluLookAt multiplies it

    // what direction are you looking
    m_look = Vector3(cos(m_yaw) * cos(m_pitch), sin(m_pitch), sin(m_yaw) * cos( m_pitch));
    Vector3 center = m_eye + m_look; // we're looking at a point in that direction
    Vector3 up = Vector3(0,1,0); // up is always...well, up
    if(m_pitch < 0) {
        up = Vector3(m_look.x, 1, m_look.z); // unless we're looking down
    }
    up.normalize();

    // tell GL the camera is at m_eye, looking at center, with up direction up
    gluLookAt(m_eye.x, m_eye.y, m_eye.z, center.x, center.y, center.z, up.x, up.y, up.z);
}

void Camera::translate(Vector3 vec) // change the position of the camera by vec
{
    m_look = Vector3(cos(m_yaw) * cos(m_pitch), sin(m_pitch), sin(m_yaw) * cos( m_pitch));
    if (m_isThirdPerson){
        m_eye =(Vector3(0,0,0)-(m_look*5)) + m_realPosition;
        m_realPosition += vec;
    } else {
        m_realPosition += vec;
        m_eye = m_realPosition;
    }
    //updateViewPlanes();
}

void Camera::setPosition(Vector3 vector){
    m_look = Vector3(cos(m_yaw) * cos(m_pitch), sin(m_pitch), sin(m_yaw) * cos( m_pitch));
    if (m_isThirdPerson){
        m_eye =(Vector3(0,0,0)-(m_look*5)) + vector;
        m_realPosition = vector;
    } else {
        m_realPosition = vector;
        m_eye = vector;
    }
    //updateViewPlanes();
}

void Camera::debugArray(float array[], int size){
    for (int i = 0; i < 4; i++){
        for (int j = 0; j < 4; j++){
            std::cout << i + j*4 << ": " << array[i + j*4] << std::flush;
        }
    }
}

//void Camera::updateViewPlanes()
//{
//    glMatrixMode(GL_MODELVIEW);
//    glPushMatrix();


//    float *projection;
//    projection = new float[16];
//    glGetFloatv(GL_PROJECTION_MATRIX, projection);
//    //std::cout << "Projection\n";
//    //debugArray(projection, 16);


//    float *view;
//    view = new float[16];
//    glGetFloatv(GL_MODELVIEW_MATRIX, view);
//    //std::cout << "View\n";
//    //debugArray(view, 16);

//    glLoadIdentity();
//    glMultMatrixf(projection);
//    glMultMatrixf(view);

//    float *res;
//    res = new float[16];
//    glGetFloatv(GL_MODELVIEW_MATRIX, res);

//    //std::cout << "Res\n";
//    //debugArray(res, 16);

//    Vector4 r0 = Vector4(res[0], res[4], res[8], res[12]);
//    Vector4 r1 = Vector4(res[1], res[5], res[9], res[13]);
//    Vector4 r2 = Vector4(res[2], res[6], res[10], res[14]);
//    Vector4 r3 = Vector4(res[3], res[7], res[11], res[15]);
//    glPopMatrix();
//    g->updateViewPlanes(r3-r0, r3+r0, r3-r1, r3+r1, r3-r2, r3+r2);
//    //std::cout << r0 << r1 << r2  << r3 << std::flush;

//}

void Camera::resize(int y, int x) {
    gluPerspective(m_fov, ((float)x)/((float)y), .1f, 90);
}

bool Camera::getMode()
{
    return m_isThirdPerson;
}

void Camera::rotate(float yaw, float pitch) // change the direction the camera is looking
{

    m_yaw += yaw;

    if (m_pitch - pitch > M_PI/2.0 - .001) {
        m_pitch = M_PI/2.0 - .001;
    } else if (m_pitch - pitch < 0 -(M_PI/2.0) + .001) {
        m_pitch = 0-(M_PI/2.0) + .001;
    } else {
        m_pitch -= pitch;
    }
}

float Camera::getYaw() // just returns m yaw
{
    return m_yaw;
}

void Camera::switchMode(){
    if (m_isThirdPerson){
        m_isThirdPerson = false;
        std::cout << "First person\n";
    } else {
        m_isThirdPerson = true;
        std::cout << "Third person\n";
    }
    translate(Vector3(0,0,0));
}
