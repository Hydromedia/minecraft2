#ifndef CYLINDERMANAGER_H
#define CYLINDERMANAGER_H

#include "engine/graphics.h"
#include "engine/manager.h"
#include "engine/entity.h"
#include "cylinder.h"
#include "shape.h"

class CylinderManager : public Manager
{
public:
    CylinderManager(Graphics *g, World *w);
    ~CylinderManager();

    Vector3 collide(Entity *e, Vector3 oldPosition, Vector3 potentialPosition);
    virtual void onTick(long seconds);
    virtual void onDraw(Graphics *g);
protected:

private:


};

#endif // CYLINDERMANAGER_H
