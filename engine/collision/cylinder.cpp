#include "cylinder.h"
#include <cmath>

Cylinder::Cylinder(Entity *e, float radius, float height) : Shape(e)
{
    m_radius = radius;
    m_height = height;
}

Cylinder::~Cylinder()
{

}

Vector3 Cylinder::collides(Shape *s)
{
    return s->collidesCylinder(this);
}

Vector3 Cylinder::collidesCylinder(Cylinder *c)
{
    Vector3 myPos = m_entity->getPosition();
    //std::cout << "Pos:" << m_entity->getPosition() << "\n";
    Vector3 cPos = c->m_entity->getPosition();

    Vector2 blue = Vector2 (myPos.x, myPos.z);
    Vector2 red = Vector2 (cPos.x, cPos.z);
    Vector3 MTV = Vector3(0, 0, 0);
    Vector3 circleMTV = Vector3(0, 0, 0);
    Vector3 lineMTV = Vector3(0, 0, 0);
    if ((blue - red).length() < m_radius+c->getRadius()){
        //std::cout << "Length: " << (blue- red).length() << "Radius: " << m_radius+c->getRadius() << "\n";
        float len = (blue - red).length();
        Vector2 vec = (((red-blue))/len) * ((m_radius + c->getRadius()) - len);
        circleMTV = Vector3(vec.x, 0, vec.y);
       //std::cout << "what.\n";
    }

    float blueMin = myPos.y;
    float blueMax = myPos.y + m_height;
    float redMax = cPos.y + c->getHeight();
    float redMin = cPos.y;
    float aRight = blueMax - redMin;
    float aLeft = redMax - blueMin;
    if (blueMin < redMax &&
        redMin < blueMax) {
        if (aLeft < 0 || aRight < 0) {
            std::cout << "Problem with Cylinders.\n";
        } if (aRight < aLeft) {
            lineMTV = Vector3(0, aRight, 0);
        } else {
            lineMTV = Vector3(0, 0 - aLeft, 0);
        }
    }
    if (circleMTV != Vector3(0, 0, 0) &&
            lineMTV != Vector3(0, 0, 0)) {
        if (circleMTV.length() < lineMTV.length()){
            //std::cout << "Circle is shorter:" << circleMTV << ", "
            //          << lineMTV << "\n";
            MTV = circleMTV;
        } else {
            MTV = lineMTV;
            //std::cout << "Line is shorter\n" << circleMTV << ", " << lineMTV << " Aright: " << aRight << " ALeft: " << aLeft <<"\n Blue: " << blueMin << ", " << blueMax << " red: " << redMin << ", " << redMax;
        }
    } else if (circleMTV != Vector3(0, 0, 0)) {
        //std::cout<< "circle" << std::flush;
    } else if (lineMTV != Vector3(0, 0, 0)) {
        //std::cout<< "Line: Me - " << m_entity->getPosition().y << " Other - " <<  c->m_entity->getPosition().y << std::flush;
    }
    return MTV;
}

float Cylinder::getRadius()
{
    return m_radius;
}

void Cylinder::setRadius(float rad)
{
    m_radius = rad;
}

float Cylinder::getHeight()
{
    return m_height;
}

void Cylinder::setHeight(float h)
{
    m_height = h;
}

void Cylinder::onDraw(Graphics *g){
    g->drawCylinder(m_entity->getPosition(), m_radius, m_radius, m_height, 100, 50);
}

