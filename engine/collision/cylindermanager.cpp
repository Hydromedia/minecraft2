#include "cylindermanager.h"

CylinderManager::CylinderManager(Graphics *g, World *w) : Manager (g, w)
{

}

CylinderManager::~CylinderManager()
{

}

Vector3 CylinderManager::collide(Entity *e, Vector3 oldPosition, Vector3 potentialPosition)
{
    for (int j = 0; j < m_world->m_entities->count(); j++){
        Entity *s = m_world->m_entities->at(j);
        if (s != e){
            e->setPosition(potentialPosition);
            Vector3 MTV = s->getShape()->collides(e->getShape());
            if (MTV != Vector3(0,0,0)) {
                //e->setPosition(oldPosition);
//                std::cout << "Colliding" << MTV << "\n";
//                //s->setPosition(s->getPosition() + ((MTV)/2.0f));
//                //e->setPosition(e->getPosition() + ((Vector3(0,0,0) - MTV)/2.0f));
                s->onCollide(e);
                e->onCollide(s);
                return potentialPosition + ((Vector3(0,0,0) - MTV)/2.0f);
            }
        } else {
            return potentialPosition;
            //std::cout << "Col me" << std::endl;
        }
    }
}

void CylinderManager::onTick(long seconds)
{

}

void CylinderManager::onDraw(Graphics *g)
{

}

