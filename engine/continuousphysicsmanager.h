#ifndef CONTINUOUSPHYSICSMANAGER_H
#define CONTINUOUSPHYSICSMANAGER_H

#include "screen.h"
#include "view.h"
#include <qgl.h>
#include "entity.h"
#include "engine/collision/shape.h"
#include "vector.h"
#include <QList>
#include "manager.h"

class ContinuousPhysicsManager : public Manager
{
public:
    ContinuousPhysicsManager(Graphics *g, World *w);
    ~ContinuousPhysicsManager();
    void onTick(float nanos);
    QList<Entity*> *m_entities;
    void collide();
};

#endif // CONTINUOUSPHYSICSMANAGER_H
