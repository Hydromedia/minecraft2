#include "entity.h"

Entity::Entity(World *w)
{
    m_world = w;
}

Entity::~Entity()
{

}

void Entity::onStaticCollide(Vector3 normal)
{

}

void Entity::onTick(float nanos){
    m_velocity = m_velocity + Vector3(m_force.x/m_mass + m_impulse.x/m_mass, m_force.y/m_mass + m_impulse.y/m_mass, m_force.z/m_mass + m_impulse.z/m_mass);
    m_position = m_world->nextPosition(this, m_position, m_position + Vector3(m_velocity.x*nanos, m_velocity.y*nanos, m_velocity.z*nanos));
    m_force =  Vector3(0, 0, 0);
    m_impulse =  Vector3(0, 0, 0);
}

void Entity::onDraw(Graphics *g){

}

Vector3 Entity::getPosition()
{
    return m_position;
}

void Entity::setPosition(Vector3 vector)
{
    m_position = vector;
}

Shape* Entity::getShape()
{
    return m_collisionShape;
}
