#include "application.h"
#include <iostream>

Application::Application(View *v, Vector2 size, Camera *c)
{
    view = v;
    m_camera = c;
    m_size = size;
    m_screenList = new QList<Screen*>();
}

Application::~Application()
{
    delete m_screenList;
}

void Application::addScreen(Screen *screen)
{
    m_screenList->push_front(screen);
}

void Application::removeTopScreen()
{
    if (m_screenList->isEmpty()){
        return;
    } else {
        m_screenList->pop_front();
    }
}

void Application::replaceScreen(Screen *newScreen)
{
    if (m_screenList->isEmpty()){
        m_screenList->push_front(newScreen);
    } else {
        m_screenList->pop_front();
        m_screenList->push_front(newScreen);
    }
}

void Application::onTick(float nanos)
{
    if (!m_screenList->isEmpty()){
        m_screenList->first()->onTick(nanos);
    }
}

void Application::onDraw(Graphics *g)
{
    g->updateView();
    //std::cout << "App Draw\n";
    if (!m_screenList->isEmpty()){
        m_screenList->first()->onDraw(g);
    }
}

void Application::onKeyPressed(QKeyEvent *event)
{
    if (!m_screenList->isEmpty()){
        m_screenList->first()->onKeyPressed(event);
    }
}

void Application::onMouseDragged(QKeyEvent *event)
{
    if (!m_screenList->isEmpty()){
        m_screenList->first()->onMouseDragged(event);
    }
}

void Application::initializeGL(Graphics *g)
{
    g->initializeGL();
}

void Application::resize(int w, int h)
{
    if (!m_screenList->isEmpty()){
        m_screenList->first()->resize(w, h);
    }
}

void Application::mousePressEvent(QMouseEvent *event)
{
    if (!m_screenList->isEmpty()){
        m_screenList->first()->mousePressEvent(event);
    }
}

void Application::mouseMoveEvent(QMouseEvent *event)
{
    if (!m_screenList->isEmpty()){
        m_screenList->first()->mouseMoveEvent(event);
    }
}

void Application::mouseReleaseEvent(QMouseEvent *event)
{
    if (!m_screenList->isEmpty()){
        m_screenList->first()->mouseReleaseEvent(event);
    }
}

void Application::wheelEvent(QWheelEvent *event)
{
    if (!m_screenList->isEmpty()){
        m_screenList->first()->wheelEvent(event);
    }
}

void Application::keyPressEvent(QKeyEvent *event)
{
    if (!m_screenList->isEmpty()){
        m_screenList->first()->keyPressEvent(event);
    }
}

void Application::keyReleaseEvent(QKeyEvent *event)
{
    if (!m_screenList->isEmpty()){
        m_screenList->first()->keyReleaseEvent(event);
    }
}
