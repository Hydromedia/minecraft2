QT += core gui opengl

TARGET = warmup
TEMPLATE = app

# If you add your own folders, add them to INCLUDEPATH and DEPENDPATH, e.g.
 INCLUDEPATH += C:\Qt\5.4\mingw491_32\glew-1.12.0\include
 DEPENDPATH += C:\Qt\5.4\mingw491_32\glew-1.12.0\include

INCLUDEPATH += src
DEPENDPATH += src



LIBS += -glut32 -glut -glew
#LIBS += -lGLU

SOURCES += src/main.cpp \
    src/mainwindow.cpp \
    src/view.cpp \
    engine/application.cpp \
    engine/camera.cpp \
    engine/graphics.cpp \
    gameworld/gamescreen.cpp \
    gameworld/menuscreen.cpp \
    engine/screen.cpp \
    gameworld/gameapplication.cpp \
    engine/entity.cpp \
    engine/world.cpp \
    engine/collision/shape.cpp \
    engine/collision/cylinder.cpp \
    gameworld/gameworld.cpp \
    gameworld/player.cpp \
    engine/voxel/voxelmanager.cpp \
    engine/voxel/chunk.cpp \
    gameworld/gamevoxelmanager.cpp \
    engine/generation/perlinnoisegenerator3d.cpp \
    engine/manager.cpp \
    engine/continuousphysicsmanager.cpp \
    engine/voxel/ray.cpp \
    engine/voxel/voxelraycollision.cpp \
    engine/collision/cylindermanager.cpp \
    gameworld/enemy.cpp

HEADERS += src/mainwindow.h \
    src/view.h \
    src/vector.h \
    engine/application.h \
    engine/camera.h \
    engine/graphics.h \
    engine/screen.h \
    gameworld/gamescreen.h \
    gameworld/menuscreen.h \
    gameworld/gameapplication.h \
    engine/entity.h \
    engine/world.h \
    engine/collision/shape.h \
    engine/collision/cylinder.h \
    gameworld/gameworld.h \
    gameworld/player.h \
    engine/voxel/voxelmanager.h \
    engine/voxel/chunk.h \
    engine/voxel/block.h \
    gameworld/gamevoxelmanager.h \
    engine/generation/perlinnoisegenerator3d.h \
    engine/manager.h \
    engine/continuousphysicsmanager.h \
    engine/voxel/ray.h \
    engine/voxel/voxelraycollision.h \
    engine/collision/cylindermanager.h \
    gameworld/enemy.h

FORMS += src/mainwindow.ui

RESOURCES += \
    texture1.qrc
