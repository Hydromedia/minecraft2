#include "gameworld.h"

#include "menuscreen.h"

GameWorld::GameWorld(Screen *screen, Camera *camera, Graphics *g): World (screen, camera, g)
{
    std::cout << "Got to GameWorld\n";
    m_screen = screen;
    voxelManager = new GameVoxelManager(m_graphics, this);
    m_managers->append(voxelManager);

    cylinderManager = new CylinderManager(m_graphics, this);
    m_managers->append(cylinderManager);
    //std::cout.setf(std::ios);

    m_player = new Player(m_screen, m_camera, this);
    this->addEntity(m_player);
}

GameWorld::~GameWorld()
{

}

Vector3 GameWorld::nextPosition(Entity *e, Vector3 oldPosition, Vector3 potentialPosition)
{
    Vector3 pos = cylinderManager->collide(e, oldPosition, potentialPosition);
    return voxelManager->collide(e, oldPosition, pos);
}


void GameWorld::onTick(float nanos){
    count+=nanos;
    winCount+=nanos;

    if (winCount >25) {
        win();
    }

    if (count >=3 && enemyCount < 1){
        Enemy *e = new Enemy(this);
        this->addEntity(e);
        enemyCount++;
        count = 0;
    }
    voxelManager->onTick(nanos);

    World::onTick(nanos);
    foreach(Entity *e, *m_entities)
    {
        e->m_force += Vector3(0, (-.5 * e->m_mass), 0);
//        if (e->getPosition().y <= 0) {
//            e->setPosition(Vector3(e->getPosition().x, 0, e->getPosition().z));
//            e->m_acceleration.y = 0;
//            e->m_velocity.y = 0;
//            e->m_force.y = 0;
//        }
    }

}

void GameWorld::lose() {
    if (m_screen){
        //m_player->setPosition(Vector3(0, 0, 0));
        m_screen->app->removeTopScreen();
        MenuScreen *gs = new MenuScreen(m_screen->app, m_screen->m_size, m_camera, QString("You Lose! Press Spacebar."), m_graphics);
        m_screen->app->addScreen(gs);
    }
}

void GameWorld::win() {
    m_screen->app->removeTopScreen();
    MenuScreen *gs = new MenuScreen(m_screen->app, m_screen->m_size, m_camera, QString("You Win! Press Spacebar."), m_graphics);
    m_screen->app->addScreen(gs);
}

void GameWorld::onDraw(Graphics *g){
    g->updateView();
    voxelManager->onDraw(g);
    World::onDraw(g);
//    glColor3f(1, 1, 1);
//    g->bindTexture("terrain");
//    g->drawRectangularPrismWithBoundAtlasTexture(1, true, -1, true, 1, true, -1, true, 1, true, -1, true, 1);
//    for (int i = 0; i < 25; i++) {
//        for (int j = 0; j < 25; j++) {
//            g->drawQuadWithBoundAtlasTexture(Vector3(i-1, 0, j-1),
//                Vector3(i-1, 0, j),
//                Vector3(i, 0, j),
//                Vector3(i, 0, j-1),
//                1);
//        }
//    }
//    g->unbindTexture();
}
void GameWorld::onKeyPressed(QKeyEvent *event) {

}

void GameWorld::onMouseDragged(QKeyEvent *event){

}

void GameWorld::resize(int w, int h){

}

void GameWorld::mousePressEvent(QMouseEvent *event){
     m_player->mousePressEvent(event);
}

void GameWorld::mouseMoveEvent(QMouseEvent *event){
    m_player->mouseMoveEvent(event);
    // This starter code implements mouse capture, which gives the change in
    // mouse position since the last mouse movement. The mouse needs to be
    // recentered after every movement because it might otherwise run into
    // the edge of the screen, which would stop the user from moving further
    // in that direction. Note that it is important to check that deltaX and
    // deltaY are not zero before recentering the mouse, otherwise there will
    // be an infinite loop of mouse move events.
}

void GameWorld::mouseReleaseEvent(QMouseEvent *event){

}

void GameWorld::wheelEvent(QWheelEvent *event){

}
void GameWorld::keyPressEvent(QKeyEvent *event){
    World::keyPressEvent(event);
    if (event->key() == Qt::Key_Escape) QApplication::quit();
    if (event->key() == Qt::Key_Backspace){
        m_screen->app->removeTopScreen();
        MenuScreen *gs = new MenuScreen(m_screen->app, m_screen->m_size, m_camera, QString("Warmup2! Press Spacebar."), m_graphics);
        m_screen->app->addScreen(gs);
    }

    m_player->keyPressEvent(event);
}

void GameWorld::keyReleaseEvent(QKeyEvent *event){
    m_player->keyReleaseEvent(event);
}

