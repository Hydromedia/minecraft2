#ifndef GAMEAPPLICATION_H
#define GAMEAPPLICATION_H

#include <engine/graphics.h>
class GameApplication : public Application
{
public:
    GameApplication(View *v, Vector2 size, Camera *c);
    ~GameApplication();
    void initializeGL(Graphics *g);
};

#endif // GAMEAPPLICATION_H
