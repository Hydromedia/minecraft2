#ifndef ENEMY_H
#define ENEMY_H
#include "engine/entity.h"
#include "engine/collision/cylinder.h"
#include "gameworld/gameworld.h"
#include "engine/voxel/ray.h"
//#include "voxelraycollision.h"

class GameWorld;
class Enemy : public Entity
{
public:
    Enemy(GameWorld *w);
    ~Enemy();
    void onStaticCollide(Vector3 normal);
    void onTick(float nanos) override;
    void onDraw(Graphics *g) override;
    void setPosition(Vector3 vector) override;
    void onCollide (Entity* e) override;
    float count = 0;
    GameWorld *world;
};

#endif // ENEMY_H
