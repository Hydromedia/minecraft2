#include "gameapplication.h"

GameApplication::GameApplication(View *v, Vector2 size, Camera *c) : Application (v, size, c)
{
}

void GameApplication::initializeGL(Graphics *g)
{
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    //loading textures

    g->loadTexture(":/images/res/grass.png", "grass");
    g->loadTexture(":/images/res/terrain.png", "terrain");
    g->m_atlasIndexAmount = ((float)16)/((float)256);
    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            g->addToTileCoordinateMap(i * 16 + j, Vector2(((float) i)*(g->m_atlasIndexAmount), ((float) j)*(g->m_atlasIndexAmount)));
        }
    }

}
