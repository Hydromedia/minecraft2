#include "menuscreen.h"
#include <QMouseEvent>
#include <QApplication>
#include "gamescreen.h"
#include <stdio.h>

MenuScreen::MenuScreen(Application *a, Vector2 size, Camera *c, QString s, Graphics *g) : Screen (a, size, c, g)
{
    m_string = s;
}

MenuScreen::~MenuScreen()
{

}

void MenuScreen::onTick(float nanos)
{

}

void MenuScreen::onDraw(Graphics *g)
{
    app->_view()->renderText(app->_view()->width()/2-70, app->_view()->height()/2-3, m_string, app->_view()->font());
}

void MenuScreen::onKeyPressed(QKeyEvent *event)
{

}

void MenuScreen::onMouseDragged(QKeyEvent *event)
{

}

void MenuScreen::initializeGL(Graphics *g)
{

}

void MenuScreen::resize(int w, int h)
{

}

void MenuScreen::mousePressEvent(QMouseEvent *event)
{

}

void MenuScreen::mouseMoveEvent(QMouseEvent *event)
{

}

void MenuScreen::mouseReleaseEvent(QMouseEvent *event)
{

}

void MenuScreen::wheelEvent(QWheelEvent *event)
{

}

void MenuScreen::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Space)
    {
        app->removeTopScreen();
        GameScreen *gs = new GameScreen(app, m_size, m_camera, m_graphics);
        app->addScreen(gs);
    }
}

void MenuScreen::keyReleaseEvent(QKeyEvent *event)
{

}

