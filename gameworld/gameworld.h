#ifndef GAMEWORLD_H
#define GAMEWORLD_H
#include "engine/world.h"
#include <qgl.h>
#include <QMouseEvent>
#include <QApplication>
#include "player.h"
#include "enemy.h"
#include "engine/voxel/voxelmanager.h";
#include "gamevoxelmanager.h";
#include "engine/collision/cylindermanager.h"

class Player;
class GameWorld : public World
{
public:
    GameWorld(Screen *screen, Camera *camera, Graphics *g);
    ~GameWorld();

    Vector3 nextPosition(Entity *e, Vector3 oldPosition, Vector3 potentialPosition);
    void onTick(float nanos) override;
    void onDraw(Graphics *g) override;
    void onKeyPressed(QKeyEvent *event) override;
    void onMouseDragged(QKeyEvent *event) override;
    void resize(int w, int h) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override ;

    void wheelEvent(QWheelEvent *event) override;
    void lose();
    void win();
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    VoxelManager *voxelManager;
    CylinderManager *cylinderManager;
    Player *m_player;
    int enemyCount = 0;
private:
    float camVeloc = 0;
    float camPos = 0;
    float count = 0;
    float winCount = 0;
};

#endif // GAMEWORLD_H
