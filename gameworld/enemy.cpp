#include "enemy.h"
#include <stdlib.h>

Enemy::Enemy(GameWorld *w) : Entity(w)
{
    world = w;
    this->m_collisionShape = new Cylinder(this, 1, 2.5);
    this->m_position = (world->m_player->getPosition()+Vector3(-10+(rand() % 20), 80, -10+(rand() % 20)));
    this->boundingAABBSize = Vector3(.4, .8, .4);
    this->m_collisionShape = new Cylinder(this, 1, 2.5);
}

Enemy::~Enemy()
{

}

void Enemy::onStaticCollide(Vector3 normal)
{

}

void Enemy::onTick(float nanos)
{
    count += nanos;
    if (count >= 1) {
        this->m_velocity+=Vector3(0,25,0);
        count = 0;
    }
    this->m_force += (world->m_player->getPosition() - this->getPosition())/100;
    Entity::onTick(nanos);

}

void Enemy::onDraw(Graphics *g)
{
    float width = boundingAABBSize.x;
    float height = boundingAABBSize.y;
    float depth = boundingAABBSize.z;
    glColor3f(.8, .2, .1);
    g->drawRectangularPrismWithBoundAtlasTexture(m_position.x-width, true, m_position.x+width, true,
                                                 m_position.z + depth, true, m_position.z - depth, true,
                                                 m_position.y+height, true, m_position.y - height, true, 14);
}

void Enemy::setPosition(Vector3 vector)
{

}

void Enemy::onCollide(Entity *e)
{

}

