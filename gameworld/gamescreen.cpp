#include "gamescreen.h"
#include <qgl.h>
#include <QTime>
#include <QTimer>
#include <QMouseEvent>
#include <QApplication>
#include <iostream>
#include <math.h>


GameScreen::GameScreen(Application *a, Vector2 size, Camera *c, Graphics *g) : Screen (a, size, c, g)
{
    //for debugging purposes
    std::ios_base::sync_with_stdio(false);
    m_world = new GameWorld(this, m_camera, m_graphics);
}

GameScreen::~GameScreen()
{
    delete m_world;
}

void GameScreen::onTick(float nanos)
{
    m_world->onTick(nanos);

}

void GameScreen::onDraw(Graphics *g)
{
    m_world->onDraw(g);
}

void GameScreen::onKeyPressed(QKeyEvent *event)
{
    m_world->onKeyPressed(event);
}

void GameScreen::onMouseDragged(QKeyEvent *event)
{
    m_world->onMouseDragged(event);
}

void GameScreen::initializeGL(Graphics *g)
{

}

void GameScreen::resize(int w, int h)
{
    m_world->resize(w,h);
}

void GameScreen::mousePressEvent(QMouseEvent *event)
{
    m_world->mousePressEvent(event);
}

void GameScreen::mouseMoveEvent(QMouseEvent *event)
{
    m_world->mouseMoveEvent(event);
}

void GameScreen::mouseReleaseEvent(QMouseEvent *event)
{
    m_world->mouseReleaseEvent(event);
}

void GameScreen::wheelEvent(QWheelEvent *event)
{
    m_world->wheelEvent(event);
}

void GameScreen::keyPressEvent(QKeyEvent *event)
{
    m_world->keyPressEvent(event);
}

void GameScreen::keyReleaseEvent(QKeyEvent *event)
{
    m_world->keyReleaseEvent(event);
}

