#include "player.h"

Player::Player(Screen *screen, Camera *camera, GameWorld *w) : Entity(w)
{
    m_camera = camera;
    //this->m_collisionShape = new Cylinder(this, 1, 2.5);
    this->setPosition(Vector3(64, 53, 64));
    this->boundingAABBSize = Vector3(.4, .8, .4);
    this->m_collisionShape = new Cylinder(this, 1, 2.5);
    m_screen = screen;
    world = w;
    chunkPositionX = (int) floor((((float)m_position.x)/((float)world->voxelManager->chunkSize)));
    chunkPositionY = (int) floor((((float)m_position.y)/((float)world->voxelManager->chunkSize)));
    chunkPositionZ = (int) floor((((float)m_position.z)/((float)world->voxelManager->chunkSize)));
    world->voxelManager->loadAndUnloadChunks(2, Vector3(chunkPositionX, chunkPositionY, chunkPositionZ));
}

Player::~Player()
{
    delete m_collisionShape;
}

void Player::onStaticCollide(Vector3 normal)
{
    if (normal.y > 0) {
        m_canJump = true;
    }
}

void Player::onTick(float nanos){

    if ((chunkPositionX != (int) floor((((float)m_position.x)/((float)world->voxelManager->chunkSize))) ||
        chunkPositionY != (int) floor((((float)m_position.y)/((float)world->voxelManager->chunkSize))) ||
        chunkPositionZ != (int) floor((((float)m_position.z)/((float)world->voxelManager->chunkSize))) &&
        world->voxelManager->hasChunk(chunkPositionX, chunkPositionY, chunkPositionZ))){

        std::cout << "Switch chunks to: " << m_position/world->voxelManager->chunkSize << std::endl;
        chunkPositionX = (int) floor((((float)m_position.x)/((float)world->voxelManager->chunkSize)));
        chunkPositionY = (int) floor((((float)m_position.y)/((float)world->voxelManager->chunkSize)));
        chunkPositionZ = (int) floor((((float)m_position.z)/((float)world->voxelManager->chunkSize)));
        world->voxelManager->loadAndUnloadChunks(2, Vector3(chunkPositionX, chunkPositionY, chunkPositionZ));
    }

    m_canJump = false;
    Entity::onTick(nanos);
    float yaw = m_camera->getYaw();
    Vector3 dir = Vector3(cos(yaw), 0, sin(yaw)); // forward-backward movement
    Vector3 perp = Vector3(dir.z, 0, -dir.x); // strafe movement
    m_direction = dir;
    m_perpendicular = perp;

    if (moveFast){
        if (m_forward) {
            m_goalVelocity += 1000*m_direction;
        } if (m_back) {
            m_goalVelocity += 1000*(-m_direction);
        } if (m_left){
            m_goalVelocity += 1000*m_perpendicular;
        } if (m_right){
            m_goalVelocity += 1000*(-m_perpendicular);
        }
    } else {
        if (m_forward) {
            m_goalVelocity += 10*m_direction;
        } if (m_back) {
            m_goalVelocity += 10*(-m_direction);
        } if (m_left){
            m_goalVelocity += 10*m_perpendicular;
        } if (m_right){
            m_goalVelocity += 10*(-m_perpendicular);
        }
    }
    //m_goalVelocity = m_direction;
    m_force += .02f* Vector3((m_goalVelocity.x - (this->m_velocity.x)), 0, (m_goalVelocity.z - (this->m_velocity.z)));
    m_goalVelocity = Vector3(0,0,0);
}

void Player::onDraw(Graphics *g){
    //std::cout << "LOOK VETOR! " << m_camera->m_look<< std::endl;
//    Vector3 center = m_camera->m_eye + m_camera->m_look*10;
//    glColor3f(0, .765, .45);
//    glLineWidth(40);
//    glBegin(GL_LINES);
//      glVertex3f(m_camera->m_eye.x, m_camera->m_eye.y, m_camera->m_eye.z);
//      glVertex3f(center.x, center.y, center.z);
//    glEnd();
//    glLineWidth(1);


    if (m_camera->getMode()){
        //m_collisionShape->onDraw(g);
        float width = boundingAABBSize.x;
        float height = boundingAABBSize.y;
        float depth = boundingAABBSize.z;
        g->drawRectangularPrismWithBoundAtlasTexture(m_position.x-width, true, m_position.x+width, true,
                                                     m_position.z + depth, true, m_position.z - depth, true,
                                                     m_position.y+height, true, m_position.y - height, true, 14);
    }
    m_camera->setPosition(Vector3(this->getPosition().x, this->getPosition().y + 2, this->getPosition().z));

    if (m_drawHelp){
        Ray ray = Ray(m_camera->m_eye, m_camera->m_look);
        VoxelRayCollision col = ray.collideChunk(world->voxelManager);
        Vector3 newBlock = col.m_voxelCollisionCoordinate;
        glColor3f(.2, .1, .871);
    //    g->drawRectangularPrismWithBoundAtlasTexture(col.m_voxelCollisionCoordinate.x-.1, true, col.m_voxelCollisionCoordinate.x+1.1, true,
    //                                                 col.m_voxelCollisionCoordinate.z + 1.1, true, col.m_voxelCollisionCoordinate.z-.1, true,
    //                                                 col.m_voxelCollisionCoordinate.y + 1.1, true, col.m_voxelCollisionCoordinate.y-.1, true, 123);
        if (col.m_blockSide.x == -1) {
            //std::cout << "Left" << std::endl;
            g->drawQuad(Vector3(newBlock.x-.01, newBlock.y, newBlock.z),
                        Vector3(newBlock.x-.01, newBlock.y+1, newBlock.z),
                        Vector3(newBlock.x-.01, newBlock.y+1, newBlock.z+1),
                        Vector3(newBlock.x-.01, newBlock.y, newBlock.z+1));
        } else if (col.m_blockSide.x == 1) {
            //std::cout << "Right" << std::endl;
            g->drawQuad(Vector3(newBlock.x+1.01, newBlock.y, newBlock.z),
                        Vector3(newBlock.x+1.01, newBlock.y, newBlock.z+1),
                        Vector3(newBlock.x+1.01, newBlock.y+1, newBlock.z+1),
                        Vector3(newBlock.x+1.01, newBlock.y+1, newBlock.z));
        } else if (col.m_blockSide.y == -1) {
            //std::cout << "Down" << std::endl;
            g->drawQuad(Vector3(newBlock.x, newBlock.y-.01, newBlock.z),
                        Vector3(newBlock.x, newBlock.y-.01, newBlock.z+1),
                        Vector3(newBlock.x+1, newBlock.y-.01, newBlock.z+1),
                        Vector3(newBlock.x+1, newBlock.y-.01, newBlock.z));
        } else if (col.m_blockSide.y == 1) {
            //std::cout << "Up" << std::endl;
            g->drawQuad(Vector3(newBlock.x, newBlock.y+1.01, newBlock.z),
                        Vector3(newBlock.x+1, newBlock.y+1.01, newBlock.z),
                        Vector3(newBlock.x+1, newBlock.y+1.01, newBlock.z+1),
                        Vector3(newBlock.x, newBlock.y+1.01, newBlock.z+1));
        } else if (col.m_blockSide.z == -1) {
            //std::cout << "Back" << std::endl;
            g->drawQuad(Vector3(newBlock.x, newBlock.y, newBlock.z-.01),
                        Vector3(newBlock.x+1, newBlock.y, newBlock.z-.01),
                        Vector3(newBlock.x+1, newBlock.y+1, newBlock.z-.01),
                        Vector3(newBlock.x, newBlock.y+1, newBlock.z-.01));
        } else {
            //std::cout << "Front" << std::endl;
            g->drawQuad(Vector3(newBlock.x, newBlock.y, newBlock.z+1.01),
                        Vector3(newBlock.x, newBlock.y+1, newBlock.z+1.01),
                        Vector3(newBlock.x+1, newBlock.y+1, newBlock.z+1.01),
                        Vector3(newBlock.x+1, newBlock.y, newBlock.z+1.01));
        }
    }

}

void Player::setPosition(Vector3 vector){
    this->m_position = vector;
    m_camera->setPosition(Vector3(this->getPosition().x, this->getPosition().y + 2, this->getPosition().z));
}

void Player::onCollide (Entity* e) {
    health--;
    std::cout << "Ouch!" << std::endl;
    if (health < 1){
        world->lose();
    }
}

void Player::keyPressEvent(QKeyEvent *event)
{
    // TODO: Handle keyboard presses here

    if(event->key() == Qt::Key_W){
        m_forward = true;
        //std::cout << "W" <<std::endl;
    }

    if(event->key() == Qt::Key_S){
        m_back = true;
        //std::cout << "S" <<std::endl;
    }

    if(event->key() == Qt::Key_A){
        m_left = true;
        //std::cout << "A" <<std::endl;
    }

    if(event->key() == Qt::Key_D){
        m_right = true;
        //std::cout << "D" <<std::endl;
    }

    if(event->key() == Qt::Key_Space) {
        if (m_canJump){
            m_position.y += .01;
            m_force.y = 1;
            m_velocity.y = 25;
        }
     }

    if(event->key() == Qt::Key_4) {
        moveFast = true;
     }
}

void Player::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_W){
        m_forward = false;
        //std::cout << "W1" <<std::endl;
        //m_goalVelocity = 100*dir;// * Vector3(m_goalVelocity.x, m_goalVelocity.y, 5);
    }

    if(event->key() == Qt::Key_S){
        m_back = false;
        //std::cout << "S1" <<std::endl;
         //m_goalVelocity = 100*(-dir);// * Vector3(m_goalVelocity.x, m_goalVelocity.y, 5);
    }

    if(event->key() == Qt::Key_A){
        m_left = false;
        //std::cout << "A1" <<std::endl;
        //m_goalVelocity = 100*perp;// * Vector3(5, m_goalVelocity.y, m_goalVelocity.z);
    }

    if(event->key() == Qt::Key_D){
        m_right = false;
        //std::cout << "D1" <<std::endl;
        //m_goalVelocity =  100*(-perp);// * Vector3(5, m_goalVelocity.y, m_goalVelocity.z);
    }
    if(event->key() == Qt::Key_G){
        m_drawHelp = !m_drawHelp;
    }

    if(event->key() == Qt::Key_4) {
        moveFast = false;
     }
}

void Player::mouseMoveEvent(QMouseEvent *event)
{
    int deltaX = event->x() - m_screen->app->_view()->width() / 2;
    int deltaY = event->y() - m_screen->app->_view()->height() / 2;
    if (!deltaX && !deltaY) return;
    QCursor::setPos(m_screen->app->_view()->mapToGlobal(QPoint(m_screen->app->_view()->width() / 2, m_screen->app->_view()->height() / 2)));

    // TODO: Handle mouse movements here
    m_camera->rotate(deltaX / 100.f, deltaY / 100.f);
}

void Player::mousePressEvent(QMouseEvent *event)
{
    Ray ray = Ray(m_camera->m_eye, m_camera->m_look);
    VoxelRayCollision col = ray.collideChunk(world->voxelManager);
    Vector3 newBlock = col.m_voxelCollisionCoordinate + col.m_blockSide;
    Block b = world->voxelManager->getBlock(col.m_voxelCollisionCoordinate.x, col.m_voxelCollisionCoordinate.y, col.m_voxelCollisionCoordinate.z);
    Vector3 chunkCoord = Vector3(floor(col.m_voxelCollisionCoordinate.x/((float)(world->voxelManager->chunkSize))),
                                 floor(col.m_voxelCollisionCoordinate.y/((float)(world->voxelManager->chunkSize))),
                                 floor(col.m_voxelCollisionCoordinate.z/((float)(world->voxelManager->chunkSize))));
    if (event->button() == Qt::LeftButton) {
        Block c = Block();
        c.type = 123;
        world->voxelManager->setBlock(newBlock.x, newBlock.y, newBlock.z, c);
        world->voxelManager->initializeBlocks(world->voxelManager->getChunk(chunkCoord.x, chunkCoord.y, chunkCoord.z));
        world->voxelManager->getChunk(chunkCoord.x, chunkCoord.y, chunkCoord.z)->resetVBO(world->m_graphics);
    } else {
        Block c = Block();
        c.type = 1;
        world->voxelManager->setBlock(col.m_voxelCollisionCoordinate.x, col.m_voxelCollisionCoordinate.y, col.m_voxelCollisionCoordinate.z, c);
        world->voxelManager->initializeBlocks(world->voxelManager->getChunk(chunkCoord.x, chunkCoord.y, chunkCoord.z));
        world->voxelManager->getChunk(chunkCoord.x, chunkCoord.y, chunkCoord.z)->resetVBO(world->m_graphics);
    }
}
void Player::setGoalVelocityX(float X)
{

}

void Player::setGoalVelocityZ(float Z)
{

}
